# Info data from simulation chains

This folder have an information about the data generated by the simulation chains:

- **PYTHIA/Angantyr + UrQMD**  

# Usage

The simulation data are located in the storage **hadrex1**:

```bash
hadrex1 (storage)

pp collision -> path: /storage/data/HIPythia/pp/

Xe-Xe collision -> path: /storage/data/HIPythia/XeXe/

Pb-Pb collsions -> path: /storage/data/HIPythia/PbPb/
```

## PYTHIA/Angantyr + UrQMD:

### ```pp collisions at 2.76 TeV```

|  **Run**  |  **Cluster(status)**  |  **Location** |     **Size**    |  **Number of events**  | **Time/job(200 events)** | **Simulation time (total)** |
|:---------:|-----------------------|---------------|:---------------:|:----------------------:|--------------------------|:---------------------------:|
|   Run-01  |  jarvis (done)        |   hadrex1     |   2.1TB         |         300M           |                          |                             |

```bash
hadrex1 (storage)
location=/storage/data/HIPythia/

pp collision -> path: /storage/data/HIPythia/pp/

#Run-01
pathtodata=/storage/data/HIPythia/pp/276TeV/Run-01/

ls -d $pathtodata/output1/output1-*.root > inputout1run01.txt
ls -d $pathtodata/output2/output2-*.root > inputout2run01.txt
ls -d $pathtodata/output3/output3-*.root > inputout3run01.txt

```


### ```pp collisions at 5.44 TeV```
|  **Run**  |  **Cluster(status)**  |  **Location** |     **Size**    |  **Number of events**  | **Time/job(200 events)** | **Simulation time (total)** |
|:---------:|-----------------------|---------------|:---------------:|:----------------------:|--------------------------|:---------------------------:|
|   Run-02  |    jarvis (done)      |    hadrex1    |   2.1 TB        |          300M          |                          |                             |

```bash
hadrex1 (storage)
location=/storage/data/HIPythia/

pp collision - path: /storage/data/HIPythia/pp/

#Run-02
pathtodata=/storage/data/HIPythia/pp/276TeV/Run-02/

ls -d $pathtodata/output1/output1-*.root > inputout1run02.txt
ls -d $pathtodata/output2/output2-*.root > inputout2run02.txt
ls -d $pathtodata/output3/output3-*.root > inputout3run02.txt

```
### ```Pb-Pb collisions at 2.76 TeV```

- ```Pb-Pb at 2.76 TeV :  7,188.030 events (done) (update 2020/05/08)```

|  **Run**  |  **Cluster(status)**  |  **Location** |     **Size**    |  **Number of events**  | **Time/job(200 events)** | **Simulation time (total)** |
|:---------:|:---------------------:|:-------------:|:---------------:|:----------------------:|:------------------------:|:---------------------------:|
|   Run-05  |      jarvis (done)    |     hadrex1   |      0.517TB    |          670934        |         7.30h            |                             |
|   Run-10  |      jarvis (done)    |     hadrex1   |      0.5TB      |          635142        |         7.08h            |                             |
|   Run-20  |      planck (done)    |     hadrex1   |      0.964TB    |          793882        |         10.31h           |           14.5 days         |
|   Run-21  |      planck (done)    |     hadrex1   |      1.1TB      |          822589        |         10.31h           |        (2 + 14.5) days      |
|   Run-22  |    jarvis (running)   |     hadrex1   |      1.09TB     |          840730        |                          |                             |
|   Run-23  |      planck (done)    |     hadrex1   |      0.584TB    |          462869        |         10.31h           |          14.5 days          |
|   Run-25  |      planck (done)    |     hadrex1   |      0.410TB    |          251681        |         10.31h           |          14.5 days          |
|   Run-27  |     planck (running)  |     hadrex1   |      0.950TB    |          771502        |         10.31h           |          14.5 days          |
|   Run-28  |     planck (running)  |     hadrex1   |      0.962TB    |          792904        |         10.31h           |          14.5 days          |
|   Run-29  |     planck (running)  |     hadrex1   |      0.441TB    |          351898        |         10.31h           |          14.5 days          |
|   Run-30  |     planck (running)  |     hadrex1   |      0.966TB    |          793899        |         10.31h           |          14.5 days          |
|   Run-31  |     planck (running)  |               |                 |                        |         10.31h           |          13 days            |
|   Run-32  |     planck (running)  |               |                 |                        |         10.31h           |          13 days            |
|   Run-33  |     planck (running)  |               |                 |                        |         10.31h           |          13 days            |
|   Run-34  |     planck (qsub)     |               |                 |                        |         10.31h           |          13 days            |

```bash
hadrex1 (storage)
location=/storage/data/HIPythia/

Pb-Pb collision -> /storage/data/HIPythia/PbPb/

#for example Run-05
pathtodata=/storage/data/HIPythia/PbPb/276TeV/Run-05/

#  get the path to all the files.root and put in one file.txt
ls -d $pathtodata/output1/output1-*.root > inputout1run05.txt
ls -d $pathtodata/output2/output2-*.root > inputout2run05.txt
ls -d $pathtodata/output3/output3-*.root > inputout3run05.txt

```

### ```Xe-Xe collisions at 5.44 TeV```
|  **Run**  |  **Cluster(status)**  |  **Location** |     **Size**    |  **Number of events**  | **Time/job(200 events)** | **Simulation time (total)** |
|:---------:|-----------------------|---------------|:---------------:|:----------------------:|--------------------------|:---------------------------:|
|   Run-06  |    jarvis (done)      |    hadrex1    |     0.077TB     |         138843         |                          |                             |
|   Run-20  |    jarvis (done)      |    hadrex1    |     0.404TB     |         400701         |                          |                             |
|   Run-26  |    centauro (done)    |    hadrex1    |     0.344TB     |         364157         |                          |                             |


```bash
hadrex1 (storage)
location=/storage/data/HIPythia/

Xe-Xe collision - /storage/data/HIPythia/PbPb/

#for example Run-20
pathtodata=/storage/data/HIPythia/XeXe/276TeV/Run-20/

#  get the path to all the files.root and put in one file.txt
ls -d $pathtodata/output1/output1-*.root > inputout1run20.txt
ls -d $pathtodata/output2/output2-*.root > inputout2run20.txt
ls -d $pathtodata/output3/output3-*.root > inputout3run20.txt

```